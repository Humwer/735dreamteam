from subprocess import check_output

PATH_TO_BIN = './ci-example.elf'

def test_numbers_25_50():
    numbers = '25 50'
    result = '750'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_125_150():
    numbers = '125 150'
    result = '2750'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_33_44():
    numbers = '33 44'
    result = '770'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_133_140():
    numbers = '133 140'
    result = '2730'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result

def test_numbers_322_1337():
    numbers = '322 1337'
    result = '16590'
    cmd = 'echo {} | {}'.format(numbers, PATH_TO_BIN)
    output = check_output(cmd, shell=True).decode()[:-1]
    assert output == result
